(function($){Drupal.behaviors.windowOpen={attach:function(context,settings){for(var i in settings.windowOpen){var selector=settings.windowOpen[i].selector;$(selector).each(function(){var link=$(this);if(link.is('a')&&!link.hasClass('window-open-processed')){link.data('windowOpenConfig',settings.windowOpen[i].config);link.addClass('window-open-processed');link.click(function(){var url=$(this).attr('href');if(url.indexOf('?')==-1){var href=url+'?popup';}else{var href=url+'&popup';}
var config=$(this).data('windowOpenConfig');var widthPat=/width=(\d+)/i;var heightPat=/height=(\d+)/i;if(popupWidth=widthPat.exec(config)){var leftPosition=($(window).width()- popupWidth[1])/ 2;
config+=',left='+ leftPosition;}
if(popupHeight=heightPat.exec(config)){var topPosition=($(window).height()- popupHeight[1])/ 2;
config+=',top='+ topPosition;}
config+='toolbar=no, location=no';if(window.location.href.indexOf('?popup')==-1||link.attr("target")=='_blank'){var newWindow=window.open(href,'_blank',config);newWindow.window.focus();}
else{window.location=href;}
return false;});}});}
if(window.location.href.indexOf('?popup')!=-1){$('form').each(function(){var action=$(this).attr('action');if(action&&action.indexOf('?popup')==-1){action+='?&popup';$(this).attr('action',action);}});}}}})(jQuery);